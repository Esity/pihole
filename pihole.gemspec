require_relative 'lib/pihole/version'

Gem::Specification.new do |spec|
  spec.name          = 'pihole'
  spec.version       = Pihole::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Pihole Gem'
  spec.description   = 'A gem to interact with Pi-hole'
  spec.homepage      = 'https://bitbucket.org/Esity/pihole/src'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/Esity/pihole/src'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/Esity/pihole/src/master/CHANGELOG.md'
  spec.metadata['bug_tracker_uri'] = 'https://bitbucket.org/Esity/pihole/issues'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'faraday'

  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
end
